import Vue from 'vue'
import App from './App.vue'
import Vuebar from 'vuebar'

Vue.config.productionTip = true;
Vue.use(Vuebar);

new Vue({
  render: h => h(App),
}).$mount('#app')
